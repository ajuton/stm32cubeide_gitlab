# Projet de démonstration du versionning pour STM32CubeIDE via GitLab.

Pour trouver la documentation nécessaire à la mise en place d'un tel site, voir https://ajuton.gitlab.io/utilisationgitlab/

## Contenu du dépôt

On trouve sur ce dépôt : 

* un projet STM32CubeIDE dont le versionning est assuré via GitLab.
* un dossier docs avec la documentation Markdown associé à ce projet.
